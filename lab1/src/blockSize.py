#!/usr/bin/env python3

from subprocess import check_output

def runtime_avg(cmd):
    total__ = 0.0
    loops__ = 5
    for i in range(loops__):
        time__ = int(check_output(cmd).decode())
        total__ += time__
    return (total__ / float(loops__))

# Block Sizes = common divisors of M=176, N=144
blksz = [1, 2, 4, 8, 16]

print ("BLKSZ", "TIME", sep="\t|\t")

time_ = 123456789
blk_ = 0

cmd = ['./a.out', '']
for blk in blksz:
    cmd[1] = str(blk)
    time = runtime_avg(cmd)
    print (blk, time, sep="\t|\t")
    if (time < time_):
        time_ = time
        blk_ = blk

print (blk_, time_)
