#!/usr/bin/env python3

from subprocess import check_output

def runtime_avg(cmd):
    total__ = 0.0
    loops__ = 5
    for i in range(loops__):
        time__ = int(check_output(cmd).decode())
        total__ += time__
    return (total__ / float(loops__))


# Haskell [x | x <- [1..n],  n `mod` x == 0]
blkszx = [1,2,3,4,6,8,9,12,16,18,24,36,48,72,144]
# Haskell [x | x <- [1..m],  m `mod` x == 0]
blkszy = [1,2,4,8,11,16,22,44,88,176]


time_ = 123456789
blkx_ = 0
blky_ = 0
cmd = ['./a.out', '', '']

print ("BLKSZX", "BLKSZY", "TIME", sep="\t\t")

for blkx in blkszx:
	cmd[1] = str(blkx)
	for blky in blkszy:
		cmd[2] = str(blky)
		time = runtime_avg(cmd)
		print (blkx, blky, time, sep="\t\t")
		if (time < time_):
			time_ = time
			blkx_ = blkx
			blky_ = blky

print (blkx_, blky_, time_)