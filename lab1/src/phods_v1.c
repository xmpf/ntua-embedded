/* Parallel Hierarchical One-Dimensional Search for motion estimation */
/* Initial algorithm - Used for simulation and profiling              */

#include <stdio.h>
#include <stdlib.h>

#include <sys/time.h> /* gettimeofday() */

#define N 144     /* Frame dimension for QCIF format */
#define M 176     /* Frame dimension for QCIF format */
#define p 7       /* Search space. Restricted in a [-p,p] region around the original location of the block. */

unsigned int Bx, By;

void read_sequence(int current[N][M], int previous[N][M])
{ 
	FILE *picture0, *picture1;
	int i, j;

	if((picture0=fopen("./akiyo0.y","rb")) == NULL)
	{
		printf("Previous frame doesn't exist.\n");
		exit(-1);
	}

	if((picture1=fopen("./akiyo1.y","rb")) == NULL) 
	{
		printf("Current frame doesn't exist.\n");
		exit(-1);
	}

	/*Input for the current frame*/
	for(i=0; i<N; i++) {
		for(j=0; j<M; j++) {
	    	previous[i][j] = fgetc(picture0);
			current[i][j] = fgetc(picture1);
    	}
  	}

	fclose(picture0);
	fclose(picture1);
}

void phods_motion_estimation(int current[N][M],
                             int previous[N][M],
                             int vectors_x[N/Bx][M/By],
                             int vectors_y[N/Bx][M/By])
{
    int x, y, k, l,
        p1, p2, q2,
        distx, disty, 
        i, 
        min1, min2, 
        bestx, besty,
        btx, bty;

    int S[] = {4, 2, 1};
    int AA[3][3] = {
                    {-4, 0, 4},
                    {-2, 0, 2},
                    {-1, 0, 1}
                 };
    int ii; /* index for S */

    int NB = N/Bx;
    int MB = M/By;
    int FFBB1 = (Bx*Bx) << 8;
    int FFBB2 = (Bx*Bx) << 8;
    
    distx = 0;
    disty = 0;

    btx = bty = 0; /* data reuse: btx = B * x, bty = B * y */

    /* For all blocks in the current frame */
    for(x=0; x < NB; x++) {
        btx = Bx * x;    /* B * x :: Data Reuse */
        for(y=0; y < MB; y++) {
            bty = By * y;    /* B * y :: Data Reuse */

            /* Loop Merge: Initialize the vector motion matrices  */
            vectors_x[x][y] = 0;
            vectors_y[x][y] = 0;

            //S = 4; => REPLACED W/ ARRAY

            /*@TODO:
            Notes:
            - while loop runs for S = {4, 2, 1}
            - data reuse {B*x, B*y} => btx,bty
            - 
            */
            ii = 0;
            while (ii < 3) {    /* LOOP MERGING */
                min1 = FFBB1;
                min2 = FFBB2;
                
                /***********************/
                i = 0; // INDEX FOR A[]
                /***********************/
                int *A = AA[ii];
                /***********************/

                // printf ("%d %d %d\n", A[0], A[1], A[2]);
                /* @TODO:
                        ...
                        -2 0 2
                        -1 0 1
                        -4 0 4
                        -2 0 2
                        -1 0 1      => Consider break them into
                        -4 0 4         parts: a. all -4,0,4
                        -2 0 2                b. all -2,0,2
                        -1 0 1                c. all -1,0,1
                        -4 0 4          together.
                        -2 0 2
                        -1 0 1
                        ...
                */

                /* For all candidate blocks in X dimension */
                while (i < 3) { /* LOOP MERGING */
                    
                    /* @TODO:
                    Notes:
                    - for loop runs for: S = {-S, 0, S}
                    - data reuse
                    */
                    
                    distx = 0;
                    disty = 0;
                    
                    /* For all pixels in the block */
                    for(k=0; k < Bx; k++) { /* LOOP MERGING */
                        for(l=0; l < By; l++) { /* LOOP MERGING */
                            
                            p1 = current[btx+k][bty+l];

                            /* synoriakes synthikes X */
                            if((btx + vectors_x[x][y] + A[i] + k) < 0 ||
                            (btx + vectors_x[x][y] + A[i] + k) >= N   ||
                            (bty + vectors_y[x][y] + l) < 0           ||
                            (bty + vectors_y[x][y] + l) >= M ) {
                                p2 = 0;
                            } 
                            else {
                                p2 = previous[btx+vectors_x[x][y]+A[i]+k][bty+vectors_y[x][y]+l];
                            }

                            /* synoriakes synthikes Y */
                            if((btx + vectors_x[x][y] + k) < 0  ||
                            (btx + vectors_x[x][y] + k) >= N    ||
                            (bty + vectors_y[x][y] + i + l) < 0 ||
                            (bty + vectors_y[x][y] + i + l) >= M ) {
                                q2 = 0;
                            } else {
                                q2 = previous[btx+vectors_x[x][y]+k][bty+vectors_y[x][y]+i+l];
                            }

                            distx += abs(p1-p2);
                            disty += abs(p1-q2);
                        }
                    }

                    // save minimum and its index
                    if(distx < min1) {
                        min1 = distx;
                        bestx = i;
                    }

                    // save minimum and its index
                    if(disty < min2) {
                        min2 = disty;
                        besty = i;
                    }

                    i++; /* A[] index */
                }

                /* S = S/2 => REPLACED WITH ARRAY S[] = {4, 2, 1} */
                vectors_x[x][y] += bestx;
                vectors_y[x][y] += besty;

                ii++; /* S[] index */
            }
        }
    }
} 

int main(int argc, char *argv[])
{  

    if (argc == 2) {
        Bx = By = atoi (argv[1]);
    } else if (argc == 3) {
        Bx = atoi (argv[1]);
        By = atoi (argv[2]);
    } else {
        //usage(argv[0]);
        exit (1);
    }

    int current[N][M],
        previous[N][M],
        motion_vectors_x[N/Bx][M/By],
        motion_vectors_y[N/Bx][M/By],
        i,j;

    struct timeval before,
                   after;

    read_sequence(current, previous);

    gettimeofday(&before, NULL);    /* get time before call */
    phods_motion_estimation(current,previous,motion_vectors_x,motion_vectors_y);
    gettimeofday(&after, NULL);     /* get time after call */

        unsigned long long diff = ( after.tv_sec*1000000 - before.tv_sec*1000000 ) \
                            + (  after.tv_usec - before.tv_usec );
    printf ("%lu\n", diff);
    return 0;
}
