/*Parallel Hierarchical One-Dimensional Search for motion estimation*/
/*Initial algorithm - Used for simulation and profiling             */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define BIG 0x1FFFFFFF
#define N 144     /*Frame dimension for QCIF format*/
#define M 176     /*Frame dimension for QCIF format*/
#define p 7       /*Search space. Restricted in a [-p,p] region around the
                    original location of the block.*/


/*********[ Global Variables ]**********/
int Bx,By;
int current[N][M];
int previous[N][M];

int distx,disty;
int i,k,l;
int min1,min2;
int bestx,besty;
int p1, p2, q2;
int x,y;

int btx,bty;
int btxk,btyl;
int vx,vxi;
int vy,vyi;
int btxkvxi,btylvy;
int btxkvx,btylvyi;
/**************************************/

void LOOP_S (int S, int vectors_x[N/Bx][M/By], int vectors_y[N/Bx][M/By]);
void LOOP_I (int i, int vectors_x[N/Bx][M/By], int vectors_y[N/Bx][M/By]);
/********* [ INLINE ] **********/
void LOOP_I (int i, int vectors_x[N/Bx][M/By], int vectors_y[N/Bx][M/By]) {
  // vxi
  vxi = vectors_x[x][y] + i;
  // vyi
  vyi = vy + i;
  distx = 0;
  disty = 0;
  /*For all pixels in the block*/
  for(k=0; k<Bx; k++) {
    // btxk
    btxk = btx + k;
    for(l=0; l<By; l++) {
      // btyl
      btyl = bty + l;
      // btxkvxi
      btxkvxi = btxk + vxi;
      // btylvy
      btylvy = btyl + vy;
      // btxkvx
      btxkvx = btxk + vx;
      // btylvyi
      btylvyi = btylvy + i;
      // replace with btx, bty
      p1 = current[btxk][btyl];

      // replace with btx, bty, btxk, vxi, vy
      if((btxkvxi) < 0 ||
          (btxkvxi) > (N-1) ||
          (btylvy) < 0 ||\
          (btylvy) > (M-1)) {
        p2 = 0;
      } else {
        p2 = previous[btxkvxi][btylvy];
      }
      // END_IF_ELSE_X

       if((btxkvx) < 0 ||
          (btxkvx) > (N-1) ||
          (btylvyi) < 0 ||\
          (btylvyi) > (M-1)) {
        q2 = 0;
      } else {
        q2 = previous[btxkvx][btylvyi];
      }
      // END_IF_ELSE_Y

      distx += abs(p1-p2);
      disty += abs(p1-q2);

    }
    // END_FOR_l
  }
  // END_FOR_k

  if(distx < min1) {
    min1 = distx;
    bestx = i;
  }
  // END_IF_X

  if(disty < min2) {
    min2 = disty;
    besty = i;
  }
  // END_IF_Y
}

void LOOP_S (int S, int vectors_x[N/Bx][M/By], int vectors_y[N/Bx][M/By]) {
  min1 = BIG;
  min2 = BIG;
  LOOP_I((-S), vectors_x, vectors_y);
  LOOP_I(0, vectors_x, vectors_y);
  LOOP_I(S, vectors_x, vectors_y);
  vectors_x[x][y] += bestx;
  vectors_y[x][y] += besty;
}
/*******************************/

void read_sequence(void)
{ 
	FILE *picture0, *picture1;
	int  j;

	if((picture0=fopen("akiyo0.y","rb")) == NULL)
	{
		printf("Previous frame doesn't exist.\n");
		exit(-1);
	}

	if((picture1=fopen("akiyo1.y","rb")) == NULL) 
	{
		printf("Current frame doesn't exist.\n");
		exit(-1);
	}

  /*Input for the previous and current frame*/
  for(i=0; i<N; i++)
  {
    for(j=0; j<M; j++)
    {
      previous[i][j] = fgetc(picture0);
      current[i][j] = fgetc(picture1);
    }
  }

	fclose(picture0);
	fclose(picture1);
}


void phods_motion_estimation(int vectors_x[N/Bx][M/By], int vectors_y[N/Bx][M/By])
{
  int j;

  distx = 0;
  disty = 0;

  /*For all blocks in the current frame*/
  for(x=0; x<N/Bx; x++)
  {
    // btx
    btx = Bx * x;
    for(y=0; y<M/By; y++)
    {
      // bty
      bty = By * y;
      // vy
      vy = vectors_y[x][y];

      LOOP_S(4, vectors_x, vectors_y);
      LOOP_S(2, vectors_x, vectors_y);
      LOOP_S(1, vectors_x, vectors_y);

    } // END_FOR_2
  } // END_FOR_1
} // END_FN_PHODS

int main(int argc, char *argv[])
{  

    if (argc == 2) { Bx = By = atoi (argv[1]); } 
    else if (argc == 3) { Bx = atoi (argv[1]); By = atoi (argv[2]); } 
    else { exit (1); }

  	int motion_vectors_x[N/Bx][M/By],
      	motion_vectors_y[N/Bx][M/By],
        j;


    /*Initialize the vector motion matrices*/
    for(i=0; i<N/Bx; i++)
    {
      for(j=0; j<M/By; j++)
      {
        motion_vectors_x[i][j] = 0;
        motion_vectors_y[i][j] = 0;
      }
    }

    struct timeval before,
                   after;

    read_sequence();

    gettimeofday(&before, NULL);
  	phods_motion_estimation(motion_vectors_x,motion_vectors_y);
  	gettimeofday(&after, NULL);     /* get time after call */

    unsigned long long diff = ( after.tv_sec*1000000 + after.tv_usec ) \
                            - (  before.tv_sec*1000000 + before.tv_usec );
    printf ("%lu\n", diff);

  	return 0;
}
