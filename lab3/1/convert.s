@ Author: Michalis Papadopoullos (03114702)
@ Homework: 3 // Exercise: 1

.text
.align 4

.global main
.type main, %function
main:
_prompt:
    mov 	r7, #4              @ write syscall, 4
    mov 	r0, #1              @ output stream to stdout (filedes: 1)
    mov 	r2, #len            @ print string length
    ldr 	r1, =prompt_str     @ string at label prompt_str:
    swi 	0                   @ execute syscall
	
_input:
	mov 	r7, #3 				@ read syscall
	ldr 	r1, =inp_str
	mov 	r2, #32 			@ input length
	mov 	r0, #0 				@ input stream (stdin filedes:0)
	swi 	0

_loop_init:
	mov 	r0, #32 			@ index
	ldr 	r1, =inp_str
	sub 	r1, r1, #1  		@ due to pre-indexing
_loop:
	ldrb 	r3, [r1, #1]! 		@ load 1 byte from mem[++r1]

_convert: 						@ _convert(r3)
	sub 	r4, r3, #'0'
	cmp 	r4, #0 				@ if number then r4 in [0 .. 9]
	blt 	_continue
	sub 	r4, r4, #9
	cmp 	r4, #0
	bgt 	_maybe_upper_
	b 		_number_

_maybe_upper_:
	sub 	r4, r3, #'A'
	cmp 	r4, #0
	blt 	_continue
	sub 	r4, r4, #25 		@ 'Z' - 'A' = 25
	cmp 	r4, #0
	bgt 	_maybe_lower_
	b 		_uppercase_

_maybe_lower_:
	sub 	r4, r3, #'a'
	cmp 	r4, #0
	blt 	_continue
	sub 	r4, r4, #25
	cmp 	r4, #0
	bgt 	_continue
	b 		_lowercase_

_number_:
	cmp 	r4, #5
	bgt 	_sub5_
	add 	r3, r3, #5
	b 		_continue
_sub5_:
	sub 	r3, r3, #5
	b 		_continue

_uppercase_:
	add 	r3, r3, #32
	b 		_continue

_lowercase_:
	sub 	r3, r3, #32
	b 		_continue

_continue:
	strb 	r3, [r1] 			@ mem[r1] = r3
	subs 	r0, r0, #1 			@ index--
	bne 	_loop 

_output:
	mov 	r7, #4 				@ read syscall
	ldr 	r1, =inp_str 		@ r1 = &inp_str[0]
	mov 	r2, #32 			@ string length
	mov 	r0, #1 				@ output stream (stdout filedes:1)
	swi 	0

_exit:  
    mov 	r7, #1              @ exit syscall
    mov 	r0, #0 				@ exit code
	swi 	0 					@ execute syscall

.data
prompt_str:     .ascii      "Enter string: "
len = . - prompt_str
inp_str:	    .ascii      "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
