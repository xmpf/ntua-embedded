.text
.global main

/*===============*/


/*===== start of count_number_of =====*/
/*	
	r0 contains the string
	r1 contains the letter to compare
	r0 returns the number of instanses of letter r1
*/	

count_number_of:
	stmfd 	sp!, {r3-r4, fp, lr}
	mov 	r4, #' '
	mov	r3, #0
	ldrb	r2, [r0]
next_number_count:
	cmp	r2, r1 /*compare current letter to given*/
	addeq	r3, r3, #1 
	streqb	r4, [r0] /*change letter to space*/
	ldrb	r2, [r0, #1]!
	cmp	r2, #0 /*check end of string*/
	bne	next_number_count
	mov 	r0, r3
	ldmfd	sp!, {r3-r4, fp, lr}
	bx lr

/*====== end of count_number_of ======*/


/*===== start of next_valid_letter =====*/
/*	
	r0 contains the string
	r0 returns the first valid letter found, 0 if no letter is found
*/	

next_valid_letter:
	stmfd 	sp!, {r1-r2, fp, lr}
	mov	r1, #0 /*r1 stores the tempotary answare*/
next_character:
	ldrb	r2, [r0], #1
	cmp	r2, #0
	beq	exit_next_valid_letter
	cmp	r2, #33 /*compare current letter to given*/
	bls	next_character
	mov	r1, r2
exit_next_valid_letter:
	mov 	r0, r1
	ldmfd	sp!, {r1-r2, fp, lr}
	bx lr

/*====== end of next_valid_letter ======*/



/*=========== start of main ==========*/
/*
	r8 stores the file openned
*/

main:

open_file:
	ldr	r0, =file_name
	ldr	r1, =01101 /* O_WRONLY|O_CREATE|O_TRUNC */
	ldr	r2, =0666
	bl	open
	
	cmp r0, #-1
	bleq file_error
		
	mov	r8, r0
	
read_string:
	ldr	r0, =output_str
	bl	printf
	ldr	r0, =inp_fmt
	ldr	r1, =inp_str
	bl	scanf
	
flush_stdin:
	/* flush stdin buffer in case more than 32 characters where given */
	/* check input length */
	ldr	r0, =inp_str
	bl	strlen
	cmp	r0, #31
	bls	check_string_validity
	/* check if string ends in new_line */
	ldr	r0, =inp_str
	ldrb	r1, [r0, #32]
	cmp	r1, #'\n'
	beq	check_string_validity

	/* while not new_line read char */
get_more_chars:	
	bl	getchar
	cmp	r0, #'\n'
	bne	get_more_chars

check_string_validity:
	ldr	r2, =inp_str
	ldrb	r0, [r2], #1
	cmp	r0, #'q'
	beq	check_validity_2
	cmp	r0, #'Q'
	beq	check_validity_2
	bl	string_valid
	
check_validity_2:
	ldrb	r0, [r2]
	cmp	r0, #0
	beq	close_file

string_valid:
	/* find valid letter and store it in r9 */
	ldr	r0, =inp_str
	bl next_valid_letter
	mov	r9, r0 /*r9 contains the letter to be found*/
	
	/* check | if string is depleted: ask new ? continue*/
	cmp	r0, #0
	bne	next_letter
	mov	r0, r8
	ldr	r1, =new_line
	mov	r2, #1
	bl	write
	bl	read_string /*read next string*/
	
next_letter:
	/* count instances of letter and replace them, store number of instances in r10 */
	ldr	r0, =inp_str
	mov	r1, r9
	bl	count_number_of
	mov	r10, r0

	/* format letter count in string print_str */
	ldr	r0, =print_str
	ldr	r1, =print_fmt
	mov	r2, r9
	mov	r3, r10
	bl	sprintf

	/* count number of letters */
	ldr	r0, =print_str
	bl	strlen
	
	/* print using open posix function */
	mov	r2, r0 /* calulated above, length of string */	
	mov	r0, r8 /* file descriptor */
	ldr	r1,=print_str
	bl	write

	bl	string_valid

close_file:
	mov	r0, r8
	bl	close
	
_exit:
	mov	r0, #0
	mov	r7, #1
	swi	0 

file_error:
	ldr 	r0, =file_error_message
	bl	printf
	bl	_exit

/*============ end of main ===========*/

.data
	random_buffer:	.ascii "\0\0"
	file_name:	.ascii "results.txt\0"
	output_str:	.ascii "Give input string:\n\0"
	num_fmt:	.ascii "%d\n\0"
	char_fmt:	.ascii "%c\n\0"
	new_line:	.ascii "\n\0"
	print_fmt:	.ascii "%c->%d\n\0"
	inp_fmt:	.ascii "%32s\0"
	inp_free_fmt:	.ascii "%s\n\0"
	print_str:	.ascii "\0\0\0\0\0\0\0\0\0\0\0\0"
	inp_str:	.ascii "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
	file_error_message: .ascii "\0File could not be opened\n\0"
	debug_message:	.ascii "in here\n\0"

