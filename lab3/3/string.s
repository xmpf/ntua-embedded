@ Author: Michalis Papadopoullos (03114702)
@ Homework: 3 // Exercise: 3

.text 			/* code section */
.align 4		/* use 32-bit ISA */

/* strcmp */
.global strcmp 	/* export symbol to linker */
.type strcmp, %function
strcmp:
	stmfd sp!, {r4, r5}		/*  r5 <>  r4 <> sp += 0x8 */
	mov r2, r0
	mov r3, #1
	mov r0, #0
strcmp_L1:
	cmp r3, #0
	beq strcmp_L2
	ldrb r0, [r2], #1 		/* load char from memloc[r2++] into r0 (postincrement r2) */
	ldrb r3, [r1], #1		/* load char from memloc[r1++] into r3 (postincrement r1) */
	subs r0, r0, r3 		/* r0 = r0 - r3 & set flags */
	beq strcmp_L1
strcmp_L2:
	ldmfd sp!, {r4, r5} 	/*  r5 <>  r4 <> sp -= 0x8 */
	mov pc, lr

/* strcpy */
.global strcpy
.type strcpy, %function
strcpy:
	mov  r2, r0 			/* r0 holds first argument */
strcpy_L1: 					/* r1 holds second argument */
	ldrb r3, [r1], #1 		/* load byte from memloc[r1++] into r3 (postincrement r1) */
	strb r3, [r2], #1 		/* store byte from r3 into memloc[r2++] (postincrement r2) */
	cmp  r3, #0
	bne  strcpy_L1
	mov  pc, lr

/* strlen */
.global strlen
.type strlen, %function
strlen:
	mov  r1, #0 			/* r1 : counter */
strlen_L1: 					/* r0 holds first argument */
	ldrb r2, [r0], #1 		/* load byte from memloc[r0++] into r2 (postincrement r0) */
	add	 r1, r1, #1 	
	cmp  r2, #0
	bne	 strlen_L1
	sub  r0, r1, #1
	mov  pc, lr

/* strcat */
.global strcat
.type strcat, %function
strcat:
	stmfd sp!, {r0-r5,  lr}		/*  lr <>  r5 <>  r4 <>  r3 <>  r2 <>  r1 <>  r0 <> sp += 0x18 */
	mov   r4, r0
	mov   r5, r1
	bl	  strlen 				/* lr = pc + 4, pc = strlen */
	add   r0, r4, r0
	mov   r1, r5
	bl	  strcpy 				/* lr = pc + 4, pc = strcpy */
	ldmfd sp!, {r0-r5, pc} 		/*  pc <>  r5 <>  r4 <>  r3 <>  r2 <>  r1 <>  r0 <> sp -= 0x18 */
