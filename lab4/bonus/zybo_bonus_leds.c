/**
 * Bonus Exercise Solution: Zybo Zynq Led Blink Project using FreeRTOS
 * References:
 * [1] https://rishifranklin.blogspot.com/2015/04/freertos-on-xilinx-zynq-zybo-single-core.html
 * [2] https://www.freertos.org/RTOS-Xilinx-Zynq.html
 * 
 * 
 *  led    | freq (DiSw OFF)  | freq (DiSw ON)
 *  -------|------------------|---------------
 *  led0   | 500ms            | 1000ms
 *  led1   | 1000ms           | 2000ms
 *  led2   | 1500ms           | 3000ms
 *  led3   | 3000ms           | 4000ms
 */

#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"           /* vTaskPrioritySet() */
#include "queue.h"          
#include "timers.h"         /* vTaskDelay() */

#include "xil_printf.h"
#include "platform.h"
#include "xparameters.h"
#include "xgpio.h"

#define LEDS_DEV      XPAR_LEDS_DEVICE_ID
#define SWITCHES_DEV  XPAR_SWITCHES_DEVICE_ID


/* Function Prototypes */
void taskBlinkLed (void *pvParameters);
void ledBlinkConfig (uint32_t ledno, uint32_t delayOff, uint32_t delayOn);

/* Global Variables */
XGpio leds;              /* LEDS => OUTPUT */
XGpio switches;          /* DIP SWITCHES => INPUT */
uint8_t current_value;   /* current value that is shown on leds */

int main (void)
{
    int ret;
    BaseType_t taskRet;

    /* TASK HANDLES */
    static TaskHandle_t led0h,      /* LED #0 */
                        led1h,      /* LED #1 */
                        led2h,      /* LED #2 */
                        led3h;      /* LED #3 */

    init_platform();

	/* Initialize the GPIO driver for the leds */
	ret = XGpio_Initialize(&leds, LEDS_DEV);
	if (ret != XST_SUCCESS) {
        xil_printf("ERROR: failed to init LEDS. Aborting\r\n");
        return XST_FAILURE;
	}    
	/* Initialize the GPIO driver for the switches */
	ret = XGpio_Initialize(&switches, SWITCHES_DEV);
	if (ret != XST_SUCCESS) {
        xil_printf("ERROR: failed to init SWITCHES. Aborting\r\n");
        return XST_FAILURE;
	}

	/* Set the direction for all led signals as outputs */
	XGpio_SetDataDirection(&leds, 1, 0);
	/* Set the direction for all switches signals as inputs */
	XGpio_SetDataDirection(&switches, 1, 1);    

    /**
     * BaseType_t xTaskCreate( TaskFunction_t pvTaskCode,
                                const char * const pcName,
                                uint16_t usStackDepth,
                                void *pvParameters,
                                UBaseType_t uxPriority,
                                TaskHandle_t *pxCreatedTask );
    * 
    **/
    
    /* TASK FOR LED #0 */
    taskRet = xTaskCreate (
        taskBlinkLed,                           /* function */
        (const char *) "TASK TO BLINK LED #1",  /* name */
        configMINIMAL_STACK_SIZE,               /* stack depth */
        (void *)0,                              /* parameters */
        0,                                      /* priority */
        &led0h                                  /* handle */
    )

    if (taskRet == pdFail) { exit (EXIT_FAILURE); }

    /* TASK FOR LED #1 */
    taskRet = xTaskCreate (
        taskBlinkLed,                           /* function */
        (const char *) "TASK TO BLINK LED #2",  /* name */
        configMINIMAL_STACK_SIZE,               /* stack depth */
        (void *)1,                              /* parameters */
        0,                                      /* priority */
        &led1h                                  /* handle */
    )

    if (taskRet == pdFail) { exit (EXIT_FAILURE); }

    /* TASK FOR LED #2 */
    taskRet = xTaskCreate (
        taskBlinkLed,                           /* function */
        (const char *) "TASK TO BLINK LED #0",  /* name */
        configMINIMAL_STACK_SIZE,               /* stack depth */
        (void *)2,                              /* parameters */
        0,                                      /* priority */
        &led2h                                  /* handle */
    )

    if (taskRet == pdFail) { exit (EXIT_FAILURE); }

    /* TASK FOR LED #3 */
    taskRet = xTaskCreate (
        taskBlinkLed,                           /* function */
        (const char *) "TASK TO BLINK LED #0",  /* name */
        configMINIMAL_STACK_SIZE,               /* stack depth */
        (void *)3,                              /* parameters */
        0,                                      /* priority */
        &led3h                                  /* handle */
    )

    if (taskRet == pdFail) { exit (EXIT_FAILURE); }


    /* Start the scheduler so the tasks start executing. */
    vTaskStartScheduler();
    
    /* 
        If all is well then main() will never reach here as the scheduler will
        now be running the tasks. If main() does reach here then it is likely that
        there was insufficient heap memory available for the idle task to be created.
    */
    return 0;
}

void taskBlinkLed (void *pvParameters)
{
    uint32_t ledno = (uint32_t)pvParameters;
    // =======================================
    configASSERT( ledno >= 0 && ledno <= 3 );
    // =======================================
    switch (ledno) {
        case 0: ledBlinkConfig (0, 500, 1000);
                break;

        case 1: ledBlinkConfig (1, 1000, 2000);
                break;

        case 2: ledBlinkConfig (2, 1500, 3000);
                break;

        case 3: ledBlinkConfig (3, 3000, 4000);
                break;

        default: XGpio_DiscreteWrite (&leds, 1, 0xF);
                 break;
    }

}

/**
 *  @ledno      =>  led position
 *  @delayOff   =>  delay when switch is off
 *  @delayOn    =>  delay when switch is on
 **/
void ledBlinkConfig (uint32_t ledno, uint32_t delayOff, uint32_t delayOn)
{
    /*
        pdMS_TO_TICKS()
            pdMS_TO_TICKS() takes a time in milliseconds as its only parameter, and evaluates
            to the equivalent time in tick periods.
        vTaskDelay()
            The number of tick interrupts that the calling task will remain in the Blocked
            state before being transitioned back into the Ready state.
    */

    const TickType_t delay_off = pdMS_TO_TICKS (delayOff);
    const TickType_t delay_on  = pdMS_TO_TICKS (delayOn);

    int switches_val = XGpio_DiscreteRead (&switches, 2);
    ledno = (2 << ledno);
    
    if ( (switches_val & ledno) == ledno ) {
        // DIP SWITCH IS PRESSED
        XGpio_DiscreteWrite (&leds, 1, (current_value |= ledno));
        vTaskDelay (delay_on);
        XGpio_DiscreteWrite (&leds, 1, (current_value &= ~ledno));
    }
    else {
        // DIP SWITCH IS NOT PRESSED
        XGpio_DiscreteWrite (&leds, 1, (current_value |= ledno));
        vTaskDelay (delay_off);
        XGpio_DiscreteWrite (&leds, 1, (current_value &= ~ledno));
    }

}