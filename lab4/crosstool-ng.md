## Crosstool-NG
Using crosstool-NG to generate a toolchain to compile ARM code from a x86 host machine.  

## Dependencies
pacstrap base base-devel grub os-prober help2man python gperf autoconf automake bison flex patch ncurses-devel git make gawk

## colorscheme
export MENUCONFIG_COLOR=mono

## Installation
git clone https://github.com/crosstool-ng/crosstool-ng  
./bootstrap  
./configure --enable-local && make  
./ct-ng list-samples  
./ct-ng menuconfig  
./ct-ng build  

## Notes
+ Set tuples alias to arm-linux. This way, we will be able to use the compiler as arm-linux-gcc.
+ A staging directory is a location into which we install all the build artifacts. We can then point future builds to this location so they can find the appropriate header and library files. A rootfs directory is a location into which we place only the files we want to have on our target.
+ The toolchain will be installed by default in $HOME/x-tools/. That’s something you could have
changed in Crosstool-ng’s configuration.

## References
+ https://www.youtube.com/watch?v=3YyH4q9lIAY
+ https://crosstool-ng.github.io/docs
